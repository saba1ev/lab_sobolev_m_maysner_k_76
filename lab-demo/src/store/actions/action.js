import axios from '../../chat-axios';

export const FETCH_CHAT_SUCCESS = 'FETCH_CHAT_SUCCESS';
export const FETCH_CHAT_REQUEST = 'FETCH_CHAT_REQUIRE';
export const FETCH_CHAT_ERROR = 'FETCH_CHAT_ERROR';
export const CHANGE_VALUE = 'CHANGE_VALUE';
export const changeValue = (event) => ({type: CHANGE_VALUE, event});


export const chatSuccess = (chat) => ({type: FETCH_CHAT_SUCCESS, chat});
export const chatRequest = () => ({type: FETCH_CHAT_REQUEST});
export const chatError = (error) => ({type: FETCH_CHAT_ERROR, error});

export const fetchChat = (date) => {
  return (dispatch) => {
        dispatch(chatRequest());
        const url = '/chat';
        const dateUrl = `/chat?datetime=${date}`;
        return axios.get(date ? dateUrl : url).then(response => {
            dispatch(chatSuccess(response.data));
        }, error => {
            dispatch(chatError(error));
        });
    }
};

export const fetchPostChat = (data) => {
    return dispatch => {
        return axios.post('/chat', data).then(() => dispatch(fetchChat()))
            .catch((e) => {
                alert(e.response.data.error)
            })
    }
};