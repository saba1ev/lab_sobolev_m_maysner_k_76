import {CHANGE_VALUE, FETCH_CHAT_ERROR, FETCH_CHAT_REQUEST, FETCH_CHAT_SUCCESS} from "../actions/action";


const initialState = {
    chat: [],
    author: '',
    message: '',
    loading: false,
    error: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_VALUE:
            return {
                ...state,
                [action.event.target.name]: action.event.target.value
            };
        case FETCH_CHAT_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_CHAT_SUCCESS:
            return {
                ...state,
                chat: state.chat.concat(action.chat),
                loading: false,
            };
        case FETCH_CHAT_ERROR:
            return {
                ...state,
                error: action.error
            };
        default:
            return state;
    }
};

export default reducer;