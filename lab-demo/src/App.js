import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import Chat from "./containers/Chat/Chat";


class App extends Component {
    render() {
        return (
                <Switch>
                    <Route path="/" component={Chat}/>
                    <Route render={() => <h1>Not found!</h1>}/>
                </Switch>
        );
    }
}

export default App;