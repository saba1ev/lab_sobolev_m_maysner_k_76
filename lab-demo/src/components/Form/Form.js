import React, {Component, Fragment} from 'react';
import {changeValue, fetchPostChat} from "../../store/actions/action";
import {connect} from "react-redux";

import './Form.css';

class Form extends Component {

    submitHandler = (event) => {
        event.preventDefault();
        const data = {
            author: this.props.state.author,
            message: this.props.state.message,
        };
        this.props.fetchPostChat(data);
    };


    render() {
        return (
            <Fragment>
                <input type="text" name='author' placeholder='please enter a Author'
                       value={this.props.state.author} onChange={this.props.changeValue}
                />
                <input type="text" name="message" placeholder='please enter a message'
                       value={this.props.state.message} onChange={this.props.changeValue}
                />
                <button
                    onClick={(event) => this.submitHandler(event)}>Send
                </button>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    state: state
});

const mapDispatchToProps = dispatch => ({
    changeValue: (event) => dispatch(changeValue(event)),
    fetchPostChat: (data) => dispatch(fetchPostChat(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Form);