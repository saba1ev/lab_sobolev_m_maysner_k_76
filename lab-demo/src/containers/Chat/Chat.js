import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchChat} from "../../store/actions/action";
import Form from "../../components/Form/Form";
import Moment from 'react-moment';

import './Chat.css';
import Spinner from "../../components/UI/Spinner/Spinner";

let interval;

class Chat extends Component {

    componentDidMount() {
        this.props.fetchChat();

        interval = setInterval(() => {
            const message = this.props.chat[this.props.chat.length - 1];
          this.props.fetchChat(message.datetime)
        }, 2000)
    }

    componentDidUpdate() {
        clearInterval(interval);
        interval = setInterval(() => {
          const message = this.props.chat[this.props.chat.length - 1];
          this.props.fetchChat(message.datetime)
        }, 2000)
    }

    render() {

        return (
            <Fragment>
                {this.props.loading ? <Spinner/> : this.props.chat.map((message, index) => {
                    return (
                      <div key={index} className='Box'>
                        <p>{message.author}: </p>
                        <p>{message.message}</p>
                        <p><Moment format='HH:mm:ss DD.MM.YYYY'>{message.datetime}</Moment></p>
                      </div>
                    )
                })}
                <Form/>
            </Fragment>
        );
    }
}


const mapStateToProps = state => ({
    chat: state.chat,
    loading: state.loading
});

const mapDispatchToProps = dispatch => ({
    fetchChat: (date) => dispatch(fetchChat(date)),

});

export default connect(mapStateToProps, mapDispatchToProps)(Chat);