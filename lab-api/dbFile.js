const fs = require('fs');


const filename = './db.json';

let chat = [];

module.exports = {
    init() {
        try {
            const fileContents = fs.readFileSync(filename);
            chat = JSON.parse(fileContents);
        } catch (event) {
            chat = [];
        }
    },
    fetchChat() {
        this.init();
        return chat.slice(chat.length - 30);
    },
    addMessage(message) {
        this.init();
        chat.push(message);
        this.save();
    },
    save() {
        fs.writeFileSync(filename, JSON.stringify(chat, null, 2))
    },
    getDate(date) {
        this.init();
        return chat.filter(message => {
            return message.datetime > date
        })
    }
};

