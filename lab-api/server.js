const express = require('express');
const cors = require('cors');
const db = require('./dbFile');
const chat = require('./app/chatRouter');

db.init();

const app = express();
app.use(cors());
app.use(express.json());

const port = 8000;

app.use('/chat', chat);

app.listen(port, () => {
    console.log(`Server started on ${port} port`)
});