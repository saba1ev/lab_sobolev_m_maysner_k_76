const express = require('express');
const db = require('../dbFile');
const nanoid = require('nanoid');

const router = express.Router();

router.get('/', (req, res) => {
    if (req.query.datetime) {
        res.send(db.getDate(req.query.datetime));
    } else {
        res.send(db.fetchChat())
    }
});

router.post('/', (req, res) => {
    if (req.body.author !== '' && req.body.message !== '') {
        const message = {
            author: req.body.author,
            message: req.body.message,
            datetime: new Date().toISOString(),
            id: nanoid()
        };
        res.send("OK");
        db.addMessage(message);
    } else {
        res.status(400).send({error: 'Author and message must be present in the request!'})
    }
});

module.exports = router;